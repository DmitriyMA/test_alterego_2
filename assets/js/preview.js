var span;
$(document).ready(function () {
	$('.form_submit').on('click', function () {
	 	var options = {backdrop:false};
	    $('.tab-content').html(span);
	    var name = $('.name').val();
	    var email = $('.email').val();
	    var task = $('.task').val().replace(/\n/g, "<br>");
	    $('.tab-content').append('name: '+name+'<br>e-mail: '+email+'<br><br>'+task+'<br><div style="clear: both;"></div>');
	    $('#myModal').modal(options);
    });
});

function handleFileSelect(evt) {
	var files = evt.target.files;

	for (var i = 0, f; f = files[i]; i++) {

		if (!f.type.match('image.*')) {
			continue;
		}

		var reader = new FileReader();

		reader.onload = (function(theFile) {
			return function(e) {
				span = document.createElement('span');
				span.innerHTML = ['<img class="thumb" height="240" width="320" style="float: left;" src="', e.target.result,
				                '" title="', theFile.name, '"/>'].join('');
			};
		})(f);

		reader.readAsDataURL(f);
	}
}

document.getElementById('files').addEventListener('change', handleFileSelect, false);