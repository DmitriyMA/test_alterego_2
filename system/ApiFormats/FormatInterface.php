<?php

namespace system\ApiFormats;

interface FormatInterface
{
	# encode
    public function encode($input): string;
    
    # decode
    public function decode(string $input): array;
}