<?php

namespace system;

class Db
{
	private $db;
	private $id;
	private $error;
	private $where;
	private $table;
	private $limit;
	private $order_by;
	private $result_query;

	function __construct ()
	{
		$db = DataBase::getInstance();
        $this->db = $db->getConnection();
		return $this;
	}

	public function insert($table, $data)
	{
		$boo = false;

		$query = "INSERT INTO $table SET ";

	    foreach ($data as $key => $value) {
	      $mas[] = "`".$key."` = '".mysqli_real_escape_string($this->db, $value)."'";
	    }

	    $query .= implode(", ", $mas);
	    if ($this->query($query) !== false) {
	    	$this->saveId();
	    	$boo = true;
	    }

	    return $boo;
	}

	public function update($table, $data)
	{
		$where = $this->where == '' ? '' : ' WHERE '.$this->where;

		$query = "UPDATE $table SET ";
		foreach ($data as $key => $value) {
			$mas[] = "`".$key."` = '".mysqli_real_escape_string($this->db, $value)."'";
		}
		$query .= implode(", ", $mas);

		$query .= $where;

		return $this->query($query);
	}

 	private function query($query) 
	{
		$query_db = mysqli_query($this->db, $query);
		
		if ($query_db === FALSE) 
		{ 
			$this->error = mysqli_error($this->db)." в запросе - $query";
		}

		$this->where = '';
		$this->table = '';
		$this->limit = '';
		$this->order_by = '';

		return $query_db;
 	}

 	public function where($key='', $if='', $value='')
 	{
 		if ($this->where != '') $this->where .= ' AND ';

 		if (is_array($key)) {
 			$i=0;
 			foreach ($key as $arr_kay => $arr_val) {
 				if ($i > 0) $this->where .= ' AND ';
 				$arr_val = mysqli_real_escape_string($this->db, $arr_val);
 				$this->where .= "`$arr_kay` = '$arr_val'";
 				$i++;
 			}
 		} else {
 			$value = mysqli_real_escape_string($this->db, $value);
 			$this->where .= " `$key` $if '$value'";
 		}

 		return $this;
 	}

 	public function orWhere($key='', $if='', $value='')
 	{
 		if ($this->where != '') $this->where .= ' OR ';

 		if (is_array($key)) {
 			$i=0;
 			foreach ($key as $arr_kay => $arr_val) {
 				if ($i > 0) $this->where .= ' OR ';
 				$arr_val = mysqli_real_escape_string($this->db, $arr_val);
 				$this->where .= "`$arr_kay` = '$arr_val'";
 				$i++;
 			}
 		} else {
 			$value = mysqli_real_escape_string($this->db, $value);
 			$this->where .= " `$key` $if '$value'";
 		}

 		return $this;
 	}

 	public function get($table)
 	{
 		$where = $this->where == '' ? '' : ' WHERE '.$this->where;
 		$this->result_query = $this->query('SELECT * FROM '.$table.$where);
 		return $this;
 	}

 	public function result()
 	{
 		$n = !$this->result_query ? 0 : mysqli_num_rows($this->result_query);
	    $mass = array();

	    for($i = 0; $i < $n; $i++)
	    {
	      $row = mysqli_fetch_object($this->result_query);    
	      $mass[] = $row;
	    }

	    return $mass;
 	}

 	public function resultArray()
 	{
 		$n = !$this->result_query ? 0 : mysqli_num_rows($this->result_query);
	    $mass = array();

	    for($i = 0; $i < $n; $i++)
	    {
	      $row = mysqli_fetch_assoc($this->result_query);    
	      $mass[] = $row;
	    }

	    return $mass;
 	}

 	public function rowArray()
 	{
 		$mass = array();
	    $n = !$this->result_query ? 0 : mysqli_num_rows($this->result_query);
		if ($n > 0) {
			$mass = mysqli_fetch_assoc($this->result_query);
		}

		return $mass;
 	}

 	private function saveId()
 	{
 		$this->id = mysqli_insert_id($this->db);
 	}

 	public function lastId()
 	{
 		return $this->id;
 	}

 	public function getError()
 	{
 		return $this->error;
 	}

}